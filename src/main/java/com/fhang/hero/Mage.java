/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.hero;

/**
 *
 * @author Natthakritta
 */
public class Mage extends Hero{

    public Mage(String name) {
        super(name, 'M', 2000);
    }
    @Override
    public void show(){
        //System.out.println("----------------------------");
        System.out.println(name+" ,Type'"+type+"'(Mage)");
        System.out.println("Blood = "+blood);
        System.out.println("----------------------------");
    }
}
