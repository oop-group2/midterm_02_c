/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.hero;

/**
 *
 * @author Natthakritta
 */
public class Main {
    public static void main(String[] args) {
        Tank tank = new Tank("Maloach");
        tank.show();
        Carry carry = new Carry("Joker");
        carry.show();
        Fighter fighter = new Fighter("Florentino");
        fighter.show();
        Support support = new Support("Annette");
        support.show();
        Mage mage = new Mage("Lauriel");
        mage.show();
        tank.setAttack(500);
        tank.show();
        carry.setAttack(500);
        carry.show();
        fighter.setAttack(500);
        fighter.show();
        support.setAttack(500);
        support.show();
        mage.setAttack(500);
        mage.show();
        tank.setAttack(200, 300);
        tank.show();
        carry.setAttack(500,500);
        carry.show();
        fighter.setAttack(200,300);
        fighter.show();
        support.setAttack(500,500);
        support.show();
        mage.setAttack(200,300);
        mage.show();
        tank.setAttack(100,100,100);
        tank.show();
        carry.setAttack(500,500,500);
        carry.show();
        fighter.setAttack(200,300,500);
        fighter.show();
        support.setAttack(500,500,500);
        support.show();
        mage.setAttack(200,300,500);
        mage.show();
        mage.setAttack(1);
        mage.show();
        mage.setAttack(200,300);
        mage.show();
        mage.setAttack(200,300,500);
        mage.show();
        
    }
}
