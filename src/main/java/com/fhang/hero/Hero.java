/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.hero;

/**
 *
 * @author Natthakritta
 */
public class Hero {

    protected String name;
    protected char type;
    protected int blood;
    private int attack;
    private int critical;
    private int mage;

    public Hero(String name, char type, int blood) {
        this.name = name;
        this.type = type;
        this.blood = blood;
    }

    public void setAttack(int attack) {
        if (blood <= 0) {
            System.out.println("!!!ERROR!!!");
        } else {
            blood = blood - attack;
            checkBloodAttack();
        }
    }

    public void setAttack(int attack, int critical) {
        if (blood <= 0) {
            System.out.println("!!!ERROR!!!");
        } else {
            blood = blood - (attack + critical);
            checkBloodAttack();
        }
    }

    public void setAttack(int attack, int critical, int mage) {
        if (blood <= 0) {
            System.out.println("!!!ERROR!!!");
        } else {
            blood = blood - (attack + critical + mage);
            checkBloodAttack();
        }
    }

    public void checkBloodAttack() {
        if (blood <= 0) {
            System.out.println(name + " Die!!!");
        }
    }

    public String getName() {
        return name;
    }

    public char getType() {
        return type;
    }

    public int getBlood() {
        return blood;
    }

    public void show() {

    }

}
