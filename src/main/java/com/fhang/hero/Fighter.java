/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.hero;

/**
 *
 * @author Natthakritta
 */
public class Fighter extends Hero{

    public Fighter(String name) {
        super(name, 'F', 5000);
    }
    @Override
    public void show(){
        //System.out.println("----------------------------");
        System.out.println(name+" ,Type'"+type+"'(Fighter)");
        System.out.println("Blood = "+blood);
        System.out.println("----------------------------");
    }
}
